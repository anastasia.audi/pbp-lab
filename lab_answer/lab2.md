# LAB 2 ANSWERS
## Apakah perbedaan antara JSON dan XML?
- JSON adalah JavaScript Object Notation. formatnya adalah self-describing yang ditulis dengan bentuk key dan value seperti pada dictionary python. Jika dilihat pada hasil lab 2, format JSON memiliki key yaitu attribute model yang suda kita buat di view.py (to, from, title, message) sedangkan value nya adalah content yang ditulis oleh user pada admin. 
- XML adalah eXtensible Markup Language. Format XML adalah informasinya ditulis di dalam tag, mirip seperti HTML namun berbeda. Ia bentuknya self-descriptive, dan memiliki struktur seperti tree, yaitu root, branch dan leaves. Pada contoh lab 2 root nya adalah model Note yang ditulis dalam bentuk tag —> `<Note>`. lalu atribut lainnya ditulis dalam bentuk tags juga seperti `<to> <from> <title> <message>`. Format penulisannya juga sama seperti HTML, yaitu konten user nya ditulis di antara open tag dan closing tag. Sebagai contoh, `<atribut> `konten user `</atribut>`. 

## Apakah perbedaan antara HTML dan XML?
- HTML adalah Hyper Text Markup Language. HTML biasa digunakan untuk membuat sebuah website yang modelnya cukup simpel, format pembuatannya adalah menggunakan tags. HTML berisi beberapa macam elemen yang akan menyampaikan informasi kepada browser bagaimana untuk menampilkan konten seperti gambar, teks, hyperlink dan lainnya. 
- XML adalah eXtensible Markup Language. XML sering digunakan untuk mendefinisikan satu set aturan untuk menyandikan dokumen dalam format yang dapat dibaca manusia dan mesin.
- yang mirip dari kedua bahasa ini adalah cara penulisan kode nya yaitu menggunakan open tag dan closing tag. Namun HTML memiliki tag yang telah ditentukan sebelumnya. Sedangkan XML memerlukan programmer untuk mendefiniskan kumpulan tag XML nya. 
- *Perbedaan* yang paling menonjol dari kedua bahasa ini adalah tujuan fokusnya, yaitu HTML berfokus pada menampilkan data, sedangkan XML berfokus pada membawa informasi. HTML memiliki banyak elemen tag yang dapat mengembangkan struktur halaman web, namun XML hanya dapat membantu untuk bertukar data di antara berbagai platform. 

```xml
<django-objects version="1.0">
<object model="lab_2.note" pk="1">
<field name="to" type="CharField">Landi</field>
<field name="from_person" type="CharField">Zain</field>
<field name="title" type="CharField">semangat PBD</field>
<field name="message" type="CharField">semangat semester 3 n pbd <3</field>
</object>
</django-objects>
```