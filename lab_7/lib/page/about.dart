import 'package:flutter/material.dart';
import '../widgets/nav-drawer.dart';

class about extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
        title: Text("about"),
        iconTheme: IconThemeData(color: Colors.black, size: 30),
        titleTextStyle: TextStyle(color: Colors.black, fontSize: 30, fontFamily: 'Futura'),
        backgroundColor: Color(0xFFFEF284)
      ),
      body: SingleChildScrollView(
        child: Column(
        children: [ 
          SizedBox(height: 50),
          title(),
          description(),
          member1(),
          member2(),
          member3(),
          member4()
          ],
      )
      )
    );
  }
}
class title extends StatelessWidget{
  @override 
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 130,
      alignment: Alignment.center,
      child: Center(
        child: 
            Text(
              'Behind the Scenes 🚪',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 50),
            ),
        )
      );
  }
}

  class description extends StatelessWidget{
    @override 
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 50,
      alignment: Alignment.center,
      child: Center(
        child: Text(
              'Meet the people behind the project.',
              textAlign: TextAlign.left,
              style: TextStyle(fontSize: 20),
            )
      )
    );
  }
}
class member1 extends StatelessWidget{
  @override 
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 250,
      alignment: Alignment.center,
      child: Center(
        child:Image.asset(
            'assets/images/member1.png',  
            width: 350,
            height: 350)
      )
    );
  }
}

class member2 extends StatelessWidget{
  @override 
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 250,
      alignment: Alignment.center,
      child: Center(
        child:Image.asset(
            'assets/images/member2.png',  
            width: 350,
            height: 350)
      )
    );
  }
}
class member3 extends StatelessWidget{
  @override 
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 250,
      alignment: Alignment.center,
      child: Center(
        child:Image.asset(
            'assets/images/member3.png',  
            width: 350,
            height: 350)
      )
    );
  }
}

class member4 extends StatelessWidget{
  @override 
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 200,
      alignment: Alignment.center,
      child: Center(
        child:Image.asset(
            'assets/images/member4.png',  
            width: 350,
            height: 350)
      )
    );
  }
}
