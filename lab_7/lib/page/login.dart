import 'package:flutter/material.dart';
import '../widgets/nav-drawer.dart';

class login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
        title: Text("login"),
        iconTheme: IconThemeData(color: Colors.black, size: 30),
        titleTextStyle: TextStyle(color: Colors.black, fontSize: 30, fontFamily: 'Futura'),
        backgroundColor: Color(0xFFFEF284)
      ),
      body: SingleChildScrollView(
        child: Column(
        children: [ 
          SizedBox(height: 100),
          title(),
          SizedBox(height: 50),
          username(),
          SizedBox(height: 50),
          password(),
          SizedBox(height: 50),
          submit()

          ],
      )
      )
    );
  }
}
class title extends StatelessWidget{
  @override 
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 130,
      alignment: Alignment.center,
      child: Center(
        child: 
        Text(
              "Sign in and let's be Friends 🕺🏻 ",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 50),
            ),)

        
      );
  }
}

class username extends StatelessWidget{
  @override 
  Widget build(BuildContext context){
    return Container(           
  width: 250,

  child: const TextField(
  style: TextStyle(height: 0), 
  decoration: InputDecoration(
    enabledBorder: OutlineInputBorder(
      borderRadius: const BorderRadius.all(Radius.circular(15.0)), 
      borderSide: BorderSide(
        width: 3.0)),
    labelText: 'Username',
  ),
  ),
);
  }
}

class password extends StatelessWidget{
  @override 
  Widget build(BuildContext context){
    return Container(           
  width: 250,

  child: const TextField(
  style: TextStyle(height: 0),
  obscureText: true, 
  decoration: InputDecoration(
    enabledBorder: OutlineInputBorder(
      borderRadius: const BorderRadius.all(Radius.circular(15.0)), 
      borderSide: BorderSide(
        width: 3.0)),
    labelText: 'Password',
  ),
  ),
);
  }
}

class submit extends StatelessWidget{
  @override 
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      height: 50,
    child:
    ElevatedButton(
              // Respond to button press
              onPressed: () { 
              },
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Color(0xFFFEF284)),
                shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)))
              ),
              child: Text(
                "Submit", 
                style: TextStyle(color: Colors.black, fontSize: 20),
              ),
    )
            );
  }
}