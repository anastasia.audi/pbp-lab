import 'package:flutter/material.dart';
import '../widgets/nav-drawer.dart';

class signup extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
        title: Text("signup"),
        iconTheme: IconThemeData(color: Colors.black, size: 30),
        titleTextStyle: TextStyle(color: Colors.black, fontSize: 30, fontFamily: 'Futura'),
        backgroundColor: Color(0xFFFEF284)
      ),
      body: SingleChildScrollView(
        child: Column(
        children: [ 
          SizedBox(height: 100),
          title(),
          SizedBox(height: 50),
          username(),
          SizedBox(height: 50),
          email(),
          SizedBox(height: 50),
          password(),
          SizedBox(height: 50),
          submit()
          ],
      )
      )
    );
  }
}
class title extends StatelessWidget{
  @override 
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 130,
      alignment: Alignment.center,
      child: const Center(
        child: 
        Text(
              "Sign up and let's be Friends 🕺🏻 ",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 50),
            ),)

        
      );
  }
}

class username extends StatelessWidget{
  @override 
  Widget build(BuildContext context){
    return Container(           
  width: 250,

  child:  TextFormField(
  style: const TextStyle(height: 0), 
  decoration: const InputDecoration(
    enabledBorder: OutlineInputBorder(
      borderRadius:  BorderRadius.all(Radius.circular(15.0)), 
      borderSide: BorderSide(
        width: 3.0)),
    labelText: 'Username',
  ),
  onSaved: (String? value) {
    // This optional block of code can be used to run
    // code when the user saves the form.
  },
  ),
);
  }
}

class email extends StatelessWidget{
  @override 
  Widget build(BuildContext context){
    return Container(           
  width: 250,

  child: TextFormField(
  style: const TextStyle(height: 0), 
  decoration: const InputDecoration(
    enabledBorder: OutlineInputBorder(
      borderRadius:  BorderRadius.all(Radius.circular(15.0)), 
      borderSide: BorderSide(
        width: 3.0)),
    labelText: 'Email',
  ),
  onSaved: (String? value) {
    // This optional block of code can be used to run
    // code when the user saves the form.
  },
  ),
);
  }
}

class password extends StatelessWidget{
  @override 
  Widget build(BuildContext context){
    return Container(           
  width: 250,
  child:  TextFormField(
  style:const TextStyle(height: 0),
  obscureText: true, 
  decoration: const InputDecoration(
    enabledBorder: OutlineInputBorder(
      borderRadius:  BorderRadius.all(Radius.circular(15.0)), 
      borderSide: BorderSide(
        width: 3.0)),
    labelText: 'Password',
  ),
  onSaved: (String? value) {
    // This optional block of code can be used to run
    // code when the user saves the form.
  },
  ),
  
);
  }
}

class submit extends StatelessWidget{
  @override 
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      height: 50,
    child:
    ElevatedButton(
              // Respond to button press
              onPressed: () { 
              },
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Color(0xFFFEF284)),
                shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)))
              ),
              child: const Text(
                "Submit", 
                style: TextStyle(color: Colors.black, fontSize: 20),
              ),
    )
            );
  }
}