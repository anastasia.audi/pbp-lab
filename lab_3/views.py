from lab_3.forms import FriendForm
from django.shortcuts import render
from lab_1.models import Friend
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    context = {} #dictionary, key: variable used in HTML
    form = FriendForm(request.POST or None)
    #if it is a POST requesta and its valid, then proceed to process the form data
    if (request.method == 'POST') and (form.is_valid()):

        friends = Friend.objects.all() #obtain all the Friend data in a form of object 
        response = {'friends': friends} #dictionary
        form.save()
        #render the lab3_index html page
        return render(request, "lab3_index.html", response)
       
    context['form'] = form
    #render the form page again if not valid
    return render(request, "lab3_form.html", context)