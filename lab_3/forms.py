from lab_1.models import Friend
from django.db import models
from django.forms import ModelForm
from lab_1.models import Friend

class FriendForm(ModelForm): #extends from the ModelForm
    class Meta(): #set the form's input
        model = Friend #form's content are Friend model's attribute from lab1
        fields = "__all__" #all the data are restored into the form