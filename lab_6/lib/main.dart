import 'package:flutter/material.dart';
import './widgets/nav-drawer.dart';
import './page/signup.dart';


void main() => runApp(MyApp());
class MyApp extends StatelessWidget {
  @override
  //the build method describes how to build the widget
  Widget build (BuildContext context) {
    return MaterialApp(
      title: 'Mutuals-lab06',
      theme: ThemeData(fontFamily: 'Futura'),
      home: MyHomePage(),
      );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
        title: Text("mutuals"),
        iconTheme: IconThemeData(color: Colors.black, size: 30),
        titleTextStyle: TextStyle(color: Colors.black, fontSize: 30, fontFamily: 'Futura'),
        backgroundColor: Color(0xFFFEF284)
      ),
      body: Column(
        children: [ 
          SizedBox(height: 50),
          logo(),
          title(),
          description(),
          create_account(),
          ],
      )
    );
  }
}

class logo extends StatelessWidget{
  @override 
  Widget build(BuildContext context){
    return Container(  
        width: double.infinity,
        height: 200,
        alignment: Alignment.center,
        child: Padding(
          padding: EdgeInsets.all(10.0),
          child:Image.asset(
            'assets/images/logo.png',  
            width: 300,
            height: 300)
        )
    );
  }
}

class title extends StatelessWidget{
  @override 
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 130,
      alignment: Alignment.center,
      child: Center(
        child: 
            Text(
              'Welcome to Mutuals 👋🏼',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 50),
            ),
        )
      );
  }
}

class description extends StatelessWidget{
    @override 
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 130,
      alignment: Alignment.center,
      child: Center(
        child: Text(
              'Mutuals is an application that \n focuses in connecting end user to \n a diverse group of people sharing \n the same interests.',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 20),
            )
      )
    );
  }
}

class create_account extends StatelessWidget{
  @override 
  Widget build(BuildContext context){
    return Container(
      height: 150,

      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Text(
              "Haven't created an account?",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 20),
            ),
            ElevatedButton(
              // Respond to button press
              onPressed: () { 
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => signup(),
                  )
                );
              },
              style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Color(0xFFFEF284)),),
              child: Text(
                "Let's be Mutuals", 
                style: TextStyle(color: Colors.black, fontSize: 20),
              ),
            )
          ]
        )
      )
    );
  }

}

