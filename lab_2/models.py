from django.db import models

class Note(models.Model):
    to = models.CharField(max_length=30)
    from_person = models.CharField(max_length=70)
    title = models.CharField(max_length=70)
    message = models.CharField(max_length=200)
# Create your models here.

