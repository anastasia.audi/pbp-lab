from django.shortcuts import render
from .models import Note
from django.http.response import HttpResponse
from django.core import serializers
# Create your views here.
def index(request):
    #call Friend model 
    notes = Note.objects.all() #take object Note from the database
    response = {'Notes': notes} #create dictionary
    #if the index is called, will render the html page
    return render(request, 'lab2.html', response) 

def xml(request):
    #change the code format into xml 
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(request):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")