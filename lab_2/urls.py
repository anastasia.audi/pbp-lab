from django.urls import path
from .views import xml, json, index

urlpatterns = [
    path('', index, name='index'), 
    path('json', json), #displaying the json format of the data
    path('xml', xml) #displaying the xml format of the data
]
