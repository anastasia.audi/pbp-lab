from django.contrib import admin
from .models import Friend

#Register Friend model
# so we can add 'friend' data using the admin dashboard
admin.site.register(Friend)