from django.db import models

# Friend model that contains name, npm, and DOB (date of birth) attributes
class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.BigIntegerField()
    date_of_birth = models.DateField()