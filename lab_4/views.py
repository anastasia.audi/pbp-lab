from django.shortcuts import render
from lab_2.models import Note
from lab_4.forms import NoteForm
from .forms import NoteForm
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    #call Friend model 
    notes = Note.objects.all() #take object Note from the database
    response = {'Notes': notes} #create dictionary
    #if the index is called, will render the html page
    return render(request, 'lab4_index.html', response) 

@login_required(login_url='/admin/login/')
def add_note(request):
    context = {}
    if (request.method == 'POST'):
        form = NoteForm(request.POST or None)
        if form.is_valid():
            form.save()
            notes = Note.objects.all()
            response = {'Notes': notes} 
            return render(request, "lab4_index.html", response)
        
    # context['form'] = form
    return render(request, "lab4_form.html", context)

@login_required(login_url='/admin/login/')
def note_list(request):
    notes = Note.objects.all()
    response = {'Notes': notes}
    return render(request, 'lab4_note_list.html', response)