from lab_2.models import Note
from django.forms import ModelForm

class NoteForm(ModelForm): #extends from the ModelForm
    class Meta(): #set the form's input
        model = Note #form's content are Friend model's attribute from lab2
        fields = "__all__"  #all the data are restored into the form
